<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function yg_magazine_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['yg_magazine_settings'] = [
    '#type' => 'details',
    '#title' => t('YG Magazine Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'bootstrap',
    '#weight' => 10,
  ];

  // Social links.
  $form['yg_magazine_settings']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_magazine_settings']['social_links']['news_update'] = [
    '#type' => 'details',
    '#title' => t('Topbar News Update'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_magazine_settings']['social_links']['news_update']['news_update_link_text'] = [
    '#type' => 'textfield',
    '#title' => t('News Update Title'),
    '#description' => t('Please enter news post title'),
    '#default_value' => theme_get_setting('news_update_link_text'),
  ];
  $form['yg_magazine_settings']['social_links']['news_update']['top_news_update_url'] = [
    '#type' => 'textfield',
    '#title' => t('News Update Url'),
    '#description' => t('Please enter your news update url'),
    '#default_value' => theme_get_setting('top_news_update_url'),
  ];
  $form['yg_magazine_settings']['social_links']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['yg_magazine_settings']['social_links']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['yg_magazine_settings']['social_links']['pinterest_url'] = [
    '#type' => 'textfield',
    '#title' => t('Pinterest url'),
    '#description' => t('Please enter your pinterest url'),
    '#default_value' => theme_get_setting('pinterest_url'),
  ];
  $form['yg_magazine_settings']['social_links']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#description' => t('Please enter your Instagram url'),
    '#default_value' => theme_get_setting('instagram_url'),
  ];
  $form['yg_magazine_settings']['social_links']['youtube_url'] = [
    '#type' => 'textfield',
    '#title' => t('Youtube'),
    '#description' => t('Please enter your Youtube url'),
    '#default_value' => theme_get_setting('youtube_url'),
  ];
  $form['yg_magazine_settings']['social_links']['linkedin_url'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin'),
    '#description' => t('Please enter your linkedin_url url'),
    '#default_value' => theme_get_setting('linkedin_url'),
  ];

  // Footer-News-Post.
  $form['yg_magazine_settings']['news_post'] = [
    '#type' => 'details',
    '#title' => t('Footer News Post'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_magazine_settings']['news_post']['news_post_title'] = [
    '#type' => 'textfield',
    '#title' => t('News Post Title'),
    '#description' => t('Please enter news post title'),
    '#default_value' => theme_get_setting('news_post_title'),
  ];
  $news_post_desc = theme_get_setting('news_post_desc');
  $form['yg_magazine_settings']['news_post']['news_post_desc'] = [
    '#type' => 'text_format',
    '#title' => t('News Post Description'),
    '#description' => t('Please enter news post description...'),
    '#default_value' => $news_post_desc['value'],
    '#foramt'        => $news_post_desc['format'],
  ];
  $form['yg_magazine_settings']['news_post']['news_post_email'] = [
    '#type' => 'email',
    '#title' => t('Email'),
    '#description' => t('Please enter news post email-id'),
    '#default_value' => theme_get_setting('news_post_email'),
  ];

  // Footer custom text.
  $form['yg_magazine_settings']['footer'] = [
    '#type' => 'details',
    '#title' => t('Footer Section'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $copyright = theme_get_setting('copyright');
  $form['yg_magazine_settings']['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];

}
